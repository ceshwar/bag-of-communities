# README #

Repo for the paper: "The Bag of Communities: Identifying Abusive Behavior Online with Preexisting Internet Data"

* Authors: Eshwar Chandrasekharan, Mattia Samory, Anirudh Srinivasan, Eric Gilbert
* Published at CHI 2017.

### What is this repository for? ###

The repo contains the scripts (in code/) and the .pkl files with pipelines trained (in models/) for the following:

* CCS Internal Estimators trained on source communities: is_4chan
* BoC static model

### Who do I talk to? ###

* Repo owner or admin: Eshwar Chandrasekharan
* Contact email: eshwar3 [at] gatech [dot] edu