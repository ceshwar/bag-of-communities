import csv
import sys
import re
import pandas as pd
import numpy as np
import nltk
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from scipy.sparse import hstack
##from lexicons.liwc import Liwc
import collections
import scipy.sparse

##liwc = Liwc()

def glue_reduce(d, thresh, cap):
    ret = []
    buf = ""
    for s in d:
        if len(s) > cap:
            continue
        elif len(s) > thresh: 
            if not buf:
                ret.append(s)
            else:
                ret.append(s + buf)
                buf = ""
        else:
            buf = buf + " " + s    
    return ret

def equalize_datasets(d1, d2):
    if len(d1) < len(d2):
        return (d1, np.random.choice(d2, size=len(d1), replace=False))
    else:
        return (np.random.choice(d1, size=len(d2), replace=False), d2)

####### To prevent MemoryError during "np.concatenate", limit the size of dataframes to some SAFE threshold
def equalize_datasets_threshold(d1, d2, thresh):
    limit = 0
    if len(d1) < len(d2) :
	if len(d1) < thresh :
		limit = len(d1)
    	else:
		limit = thresh
    else:
	if len(d2) < thresh :
		limit = len(d2)
	else:
		limit = thresh

    if len(d1) < limit:
	if len(d2) < limit:
		return (d1,d2)
	else:
		 return (d1, np.random.choice(d2, size=limit, replace=False))
    else:
	if len(d2) < limit:
		return (np.random.choice(d1, size=limit, replace=False), d2)
	else:
		return (np.random.choice(d1, size=limit, replace=False), np.random.choice(d2, size=limit, replace=False))
######


def normalize_replies(d):
    return [re.sub(r'>>\d+\s+', "", s) for s in d]

def normalize_repeats(d):
    d1 = [re.sub(r'(\w)\1\1\1+', r'\1', s) for s in d] # single letters that repeat 3 more times
    d2 = [re.sub(r'(\w{2})\1\1+', r'\1', s) for s in d1] # two letters that repeat 2 or more times
    d3 = [re.sub(r'(\w{3})\1+', r'\1', s) for s in d2] # three letters that repeat at least once
    return d3

### adding new function
### Remove all the  lines/posts that have no tokens in them

def normalize_nonTokenizable(d):
	temp_token_pattern=r"[a-z]['a-z]*"
	#temp_sentence_count = len(re.findall(r"[.!?]+", s)) or 1
	# tokens is a bit redundant because it duplicates the tokenizing done
	# in read_document, but to keep read_document simple, we just run it again here.
	#temp_tokens = re.findall(temp_token_pattern, s.lower())
	#if len(temp_tokens) = 0, then remove that line!
	ret = []
        for s in d:
		if len(re.findall(temp_token_pattern,s.lower())) > 0 :
			ret.append(s)
		else :
			continue
	return ret
### end of new function

def append_pos_tags(d):
    for s in d:
        buf = str(s.decode('ascii', 'replace').replace(u'\ufffd', '_'))
        text = nltk.wordpunct_tokenize(buf)
        for (text, tag) in nltk.pos_tag(text):
            buf = buf + " " + tag
        ret.append(buf)
    return ret

def liwc_ify(d):
    ret = []
    for s in d: 
       liwc_sum = liwc.summarize_document(s)
       liwc_sum = collections.OrderedDict(sorted(liwc_sum.items()))
       # liwc_sum.pop('WC') # don't want it learning length
       # row = liwc_sum.values()
       ret.append([liwc_sum['WPS'], liwc_sum['AllPct']])
    return scipy.sparse.csr_matrix(ret)

#Input is given through the command line:>>  *.py *.txt *.txt
#could use either of metafilter or 4chan as the varfile and controlfile
varfile = sys.argv[1]
controlfile = sys.argv[2]

##print "Loading File 1"
##vardf = pd.read_csv(varfile)
##print "Loading File 2"
##controldf = pd.read_csv(controlfile)

#### Start of debugging
#Specify the number of rows in the file that need to be read - To prevent MemoryError in case of large files!
rows_thresh = 500000
print "Loading File 1"
vardf = pd.read_csv(varfile, nrows = rows_thresh)
print "Loading File 2"
controldf = pd.read_csv(controlfile, nrows = rows_thresh)
##

print "Number of rows in datasets:"
print len(vardf)
print len(controldf)
#Need to control for size of the array

##### End of debugging

print "Normalize text for replies"
# normalize some of the text (experimental)
var_normalized = normalize_replies(np.array(vardf['text'].values, dtype=str))
control_normalized = normalize_replies(np.array(controldf['text'].values, dtype=str))

##### Start of normalization addition

print "Normalize for non-tokenizable sentences"
var_normalized = normalize_nonTokenizable(var_normalized)
control_normalized = normalize_nonTokenizable(control_normalized)

##### End of normalization addition

# concat until every data point is greater than threshold length
length_threshold = 20
length_cap = 20000
var_reduced = glue_reduce(var_normalized, length_threshold, length_cap)
control_reduced = glue_reduce(control_normalized, length_threshold, length_cap)

print "Downsample datasets"
# downsample datasets until the same size
# original
(var_final, control_final) = equalize_datasets(var_reduced, control_reduced)
#

### start of changes
###modify to ensure size of datasets are not too big to cause MemoryError
###specify some temporary threshold for size of dataframes
##dataframe_thresh = 140000
##(var_final, control_final) = equalize_datasets_threshold(var_reduced, control_reduced, dataframe_thresh)
#### end of changes

print "Contructing dataframes for datasets with size: %d and %d" % (len(var_final), len(control_final))

y1 = np.repeat(1, len(var_final))
y2 = np.repeat(0, len(control_final))
y = np.concatenate((y1, y2), axis=0)
x = np.concatenate((var_final, control_final), axis=0)
x = np.array(x, dtype=str)

# start hard-coded NB experiments

def experiment(df, nmax, clf, use_binary=True):
    print "---- begin experiment ----"
    print "vectorizing %d-grams with %d min df ..." % (nmax, df)
    vectorizer_words = CountVectorizer(min_df=df, analyzer='word', ngram_range=(1,nmax), binary=use_binary, lowercase=True)
    x_words = vectorizer_words.fit_transform(x)
    
    ## Experiment with just the words present - ngrams
    print "vocabulary size: %d" % len(vectorizer_words.get_feature_names())
    scores = cross_val_score(clf, x_words, y, cv=10)
    print "10-fold cv accuracy (words only):"
    print scores.mean()
    print ""

    # print "vectorizing 3-chars with %d min df ..." % df
    # vectorizer_char = CountVectorizer(min_df=df, analyzer='char', ngram_range=(1,3), binary=use_binary, lowercase=False)
    # x_chars = vectorizer_char.fit_transform(x)
    # print "vocabulary size: %d" % len(vectorizer_char.get_feature_names())

    # scores = cross_val_score(clf, x_chars, y, cv=10)
    # print "10-fold cv accuracy (chars only):"
    # print scores.mean()
    # print ""

    ##print "liwc-ifying text ..."
    ##x_liwc = liwc_ify(x)

    ##print "stacking words and liwc ..."
    ##x_both = hstack((x_words, x_liwc))
    ##print "vocabulary size: %d" % x_both.shape[1]
    ##scores = cross_val_score(clf, x_both, y, cv=10)
    ##print "10-fold cv accuracy (words+chars):"
    ##print scores.mean()
    ##print ""

    print "---- end experiment ----"
    print ""

c = MultinomialNB(alpha=0.01)
#c = RandomForestClassifier()
experiment(df=10, nmax=2, clf=c)
# experiment(df=10, nmax=3, clf=c)
# experiment(df=50, nmax=2, clf=c)
# experiment(df=50, nmax=3, clf=c)
