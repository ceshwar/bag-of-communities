f = "rest_of_data/da-banned-dump.txt"
count = 0
bans = [0,0]

#2 types of bans: <comment reason="hidden_by_admin"> &&& <comment reason="violation">
unique_comments = []
ban_admin = "da-uniq-admin.txt"
ban_violation = "da-uniq-violation.txt"
all_banned = "da-uniq-all-banned.txt"
write_admin = open(ban_admin,"w")
write_violation = open(ban_violation, "w")
write_banned = open(all_banned, "w")

#Each comment starts with <comment reason = "">  and ends with </comment>
num_comments = 0

with open(f, "r") as fin:
	comment = ""
	for line in fin:
		#print str(count) + ": " + line
		#count = count + 1
		#if count > limit:
			#break
		if "comment reason=\"hidden_by_admin\"" in line:
			#print "Admin!!!"
			flag = 1
			bans[0] = bans[0] + 1
			continue

		if "comment reason=\"violation\"" in line:
			#print "Violation!!!"
			flag = 2
			bans[1] = bans[1] + 1
			continue
	
		if "<br />" in line:
			#its a line break
			if line == "<br />\n":
				#print line
				continue
			#line.replace("<br />\n","")
			line = line[:-len("<br />\n")]			
			comment = comment + line
			continue
		
		if "</comment>" in line:
			#End of Comment
			#print "End of comment" 
			num_comments = num_comments + 1
			# Check if the comments aggregate is unique
			if(comment not in unique_comments):
				unique_comments.append(comment)
				#print comment
				if(flag == 1):
					write_admin.write(comment.lower()+"\n")
				else:
					write_violation.write(comment.lower()+"\n")
				write_banned.write(comment.lower()+"\n")
			comment = ""
			flag = 0
			continue

		comment = comment + line

write_admin.close()
write_violation.close()

print "No. of admin bans = " , bans[0]
print "No. of violation bans = ", bans[1]
print "Sum = ", bans[0]+bans[1]
print "Total no. of comments = ", num_comments
print "Unique comments = ", len(unique_comments)


