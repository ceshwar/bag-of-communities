import csv
import sys
import re
import pandas as pd
import numpy as np
import nltk
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from scipy.sparse import hstack
from lexicons.liwc import Liwc
import collections
import scipy.sparse
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.externals import joblib


#liwc = Liwc()

def glue_reduce(d, thresh, cap):
    ret = []
    buf = ""
    for s in d:
        if len(s) > cap:
            continue
        elif len(s) > thresh: 
            if not buf:
                ret.append(s)
            else:
                ret.append(s + buf)
                buf = ""
        else:
            buf = buf + " " + s    
    return ret

def equalize_datasets(d1, d2):
    if len(d1) < len(d2):
        return (d1, np.random.choice(d2, size=len(d1), replace=False))
    else:
        return (np.random.choice(d1, size=len(d2), replace=False), d2)

####### To prevent MemoryError during "np.concatenate", limit the size of dataframes to some SAFE threshold
def equalize_datasets_threshold(d1, d2, thresh):
    limit = 0
    if len(d1) < len(d2) :
	if len(d1) < thresh :
		limit = len(d1)
    	else:
		limit = thresh
    else:
	if len(d2) < thresh :
		limit = len(d2)
	else:
		limit = thresh

    if len(d1) < limit:
	if len(d2) < limit:
		return (d1,d2)
	else:
		 return (d1, np.random.choice(d2, size=limit, replace=False))
    else:
	if len(d2) < limit:
		return (np.random.choice(d1, size=limit, replace=False), d2)
	else:
		return (np.random.choice(d1, size=limit, replace=False), np.random.choice(d2, size=limit, replace=False))
######


def normalize_replies(d):
    return [re.sub(r'>>\d+\s+', "", s) for s in d]

def normalize_repeats(d):
    d1 = [re.sub(r'(\w)\1\1\1+', r'\1', s) for s in d] # single letters that repeat 3 more times
    d2 = [re.sub(r'(\w{2})\1\1+', r'\1', s) for s in d1] # two letters that repeat 2 or more times
    d3 = [re.sub(r'(\w{3})\1+', r'\1', s) for s in d2] # three letters that repeat at least once
    return d3

### adding new function
### Remove all the  lines/posts that have no tokens in them

def normalize_nonTokenizable(d):
	temp_token_pattern=r"[a-z]['a-z]*"
	#temp_sentence_count = len(re.findall(r"[.!?]+", s)) or 1
	# tokens is a bit redundant because it duplicates the tokenizing done
	# in read_document, but to keep read_document simple, we just run it again here.
	#temp_tokens = re.findall(temp_token_pattern, s.lower())
	#if len(temp_tokens) = 0, then remove that line!
	ret = []
        for s in d:
		if len(s) < 2:
			continue
		if len(re.findall(temp_token_pattern,s.lower())) > 0 :
			ret.append(s)
		else :
			continue
	return ret
### end of new function

def append_pos_tags(d):
    for s in d:
        buf = str(s.decode('ascii', 'replace').replace(u'\ufffd', '_'))
        text = nltk.wordpunct_tokenize(buf)
        for (text, tag) in nltk.pos_tag(text):
            buf = buf + " " + tag
        ret.append(buf)
    return ret

def liwc_ify(d):
    ret = []
    for s in d: 
       liwc_sum = liwc.summarize_document(s)
       liwc_sum = collections.OrderedDict(sorted(liwc_sum.items()))
       # liwc_sum.pop('WC') # don't want it learning length
       # row = liwc_sum.values()
       ret.append([liwc_sum['WPS'], liwc_sum['AllPct']])
    return scipy.sparse.csr_matrix(ret)



#MAIN()

#INPUT FILES FOR TRAINING THE MODEL

#vardf = pd.read_csv(varfile, nrows = rows_thresh)
controlfile = '../data/all-4chan-09-10.txt'
#varfile = '../data/all-mf.txt'
varfile = '../rest_of_data/reddit-gilded.txt'
#varfile2 = '../data/final-undeleted-da-train.txt'
#varfile2 = '../data/latest-da-undeleted-train.txt'
#varfile2 = "../data/crawled-2mill-undeleted-da-comments.txt"
#varfile2 = '../data/all-mf.txt'
#varfile2 = '../undeleted_4million_da_comments.txt'
#varfile2 = '../undeleted_4million_da_comments.txt'
varfile2 = "dirty-undeleted_4million_da_comments.txt"

index = 0 
inputfile = []
inputfile.append(controlfile)
inputfile.append(varfile)
inputfile.append(varfile2)
print "Loading test data!"

#INITIALIZE THE CLASSIFIER
clf = MultinomialNB(alpha=0.01)
#clf = MultinomialNB(0.001)

load = 0
#to train from scratch, load = 0
#to load from disk, load = 1

#if this exceeds a limit, stop training from that sample - ensures that we train on same (similar) amount of lines from each sample!
row_limit = 1500000
#row_limit = 100000

print "Initializing hashvectorizer"

# INITIALIZE THE HASHING VECTORIZER: PARAMETERS TO CHANGE = ngram_range, lowercase, tokenizer

## Specify "tokenizer" to one which accounts for punctuations along with words
#Reference = http://stackoverflow.com/questions/32128802/how-to-use-sklearns-countvectorizerand-to-get-ngrams-that-include-any-punctua
from nltk.tokenize import TreebankWordTokenizer
#vectorizer = HashingVectorizer(decode_error='ignore',ngram_range = (1,3),lowercase = True, n_features=2 ** 18,non_negative=True, tokenizer=TreebankWordTokenizer().tokenize)
#vectorizer = HashingVectorizer(decode_error='ignore',ngram_range = (1,3),lowercase = False, n_features=2 ** 18,non_negative=True, tokenizer=TreebankWordTokenizer().tokenize)
vectorizer = HashingVectorizer(decode_error='ignore',ngram_range = (2,2),lowercase = False, n_features=2 ** 18,non_negative=True, tokenizer=TreebankWordTokenizer().tokenize)
#vectorizer = HashingVectorizer(decode_error='ignore',ngram_range = (2,2),lowercase = True, n_features=2 ** 18,non_negative=True)
#vectorizer = HashingVectorizer(decode_error='ignore',ngram_range = (2,2),lowercase = True, n_features=2 ** 18,non_negative=True)
#vectorizer = HashingVectorizer(decode_error='ignore',ngram_range = (2,2),lowercase = False, n_features=2 ** 18,non_negative=True)
#vectorizer = HashingVectorizer(decode_error='ignore',ngram_range = (1,3),lowercase = True, n_features=2 ** 18,non_negative=True)

for index in range(0,3):
  #vardf = pd.read_csv(varfile, chunksize = 50000)
  if index == 1:
 	continue

  vardf = pd.read_csv(inputfile[index], chunksize = 100000) 
  times = 0
  first = 1
  class_label = index

  if index == 2:
	class_label = 1
  
  print "File is: " + inputfile[index]
  print "Class label = ", class_label

  all_classes = np.array([0,1])
  tot_rows = 0
  #len_threshold = 25
  len_threshold = 50
  #len_threshold = 100
	
  #LOAD TRAINING DATA CHUNK BY CHUNK AND PARTIAL-FIT THE MODEL ON THIS CHUNK  

  for chunk in vardf:
	#print chunk
	#if times > 10:
	#	break
	#if True:
	#	print "Exit asap!"
	#	break
	#tot_rows = tot_rows + 100000
	
	if tot_rows >= row_limit:
		break
	#print "Chunk #",times
	times = times + 1
	temp_data = np.array(chunk['text'].values, dtype = str)
	temp_normalized = normalize_replies(temp_data)
	temp_data = []
	temp_normalized = normalize_nonTokenizable(temp_normalized)
	# concat until every data point is greater than threshold length
	#length_threshold = 20
	#length_cap = 20000
	###glue reduce
	#temp_reduced = glue_reduce(temp_normalized, length_threshold, length_cap)
	temp_reduced = temp_normalized
	temp_normalized = []

	###
	#normalize for length
	#for line in  temp_reduced:
	#	if len(line) < len_threshold:
	#		continue
	#	temp_normalized.append(line)

	#temp_reduced = temp_normalized
	###

	#if first == 1:
	#	mf_data = temp_reduced
	#	first = 0
	#	continue

	#try:
	#	mf_data = np.append(mf_data, temp_reduced)
	#except:
	#print "out of memory - Do partial fit"
	#print "Do partial fit"
	mf_data = temp_reduced
	#print len(mf_data)
	temp_reduced = []
	tot_rows = tot_rows + len(mf_data)
	#y_train = np.repeat(1, len(mf_data))
	y_train = np.repeat(class_label, len(mf_data))
	x_train = vectorizer.transform(mf_data)
	clf.partial_fit(x_train, y_train, classes = all_classes)
	##
	mf_data = []
	##
	#break
	##
 #tranform the test data and then use
 #clf.score(X_test, y_test)
	#if tot_rows >= row_limit:
	#	break

  print "Total rows from file used for training = ", tot_rows


#Model has been trained using partial fit on MF and 4chan data!
print "Training Complete!"
##print "vocabulary size: %d" % len(vectorizer.get_feature_names())
#clf_filename = 'trained_classifier.joblib.pkl'
#_ = joblib.dump(clf, clf_filename, compress=9)
#vectorizer_filename = 'vectorizer.joblib.pkl'
#_ = joblib.dump(vectorizer, vectorizer_filename, compress = 9)

#if(load == 1):
 #print "Loading classifier from memory"
 #clf = joblib.load(clf_filename)
 #vectorizer = joblib.load(vectorizer_filename)

print "vocabulary is", vectorizer
print "Time for tests!"
print clf

print "Loading Test files"

#testfile_var = "../data/clean_banned_da_comments.txt"
testfile_var = "../data/da-uniq-all-banned.txt"
#testfile_control = "../data/uniq-undeleted-da-test.txt"
#testfile_control = "../data/final-undeleted-da-test.txt"
#testfile_control = '../data/latest-da-undeleted-test.txt'
testfile_control = '../data/goyya-da-uniq-undeleted.txt'
##
#read deleted da-comments
testfile = []
testfile.append(testfile_var)
testfile.append(testfile_control)
#row_limit = 5000

#counts for test elements of each class
total_test_count = [0,0]
confident_test_count = [0,0]
confidence_threshold = [0.7,0.5]

for index in range(0,2):
 x_test_words = []
 y_test = []
 scores = 0.0
 denom = 0
 testdf = pd.read_csv(testfile[index], chunksize = 5000)
 print testfile[index]
 times = 0
 tot_rows = 0
 for chunk in testdf:
        #print chunk
        #if times > 10:
        #       break
        #if True:
        #       print "Exit asap!"
        #       break

	#tot_rows = tot_rows + 5000
        #print "Chunk #",times
        times = times + 1
        temp_data = np.array(chunk['text'].values, dtype = str)
	temp_normalized = normalize_replies(temp_data)
        temp_data = []
        test_data = normalize_nonTokenizable(temp_normalized)
        temp_normalized = []
	##Remove lines with less than 50 characters
	#len_threshold = 50
	#len_threshold = 100
	###

	for line in test_data:
		if len(line) >= len_threshold:
			temp_normalized.append(line)

	test_data = temp_normalized
	
        #print len(test_data)
	tot_rows = tot_rows + len(test_data)
	y_test = np.repeat(index, len(test_data))
        x_test = vectorizer.transform(test_data)
	score = clf.score(x_test,y_test)
	#
	#print "Test results:"
	#print score
	#print("Accuracy: %0.2f (+/- %0.2f)" % (score.mean(), score.std() * 2))
	#print ""
	scores = scores + score
	denom = denom + 1
	
	#if tot_rows >= row_limit:
	#	break
	#
        
	##estimate probabilties of belonging to each class for the test data
        #probs = clf.predict_proba(x_test)
        #print "Length of predictions = ", len(probs)
        #for prob in probs:
        #       print "Class 0 = ", prob[0]
        #        print "Class 1 = ", prob[1]

	#total_test[index] = total_test[index] + len(x_test)
	#for prob in probs:
	#	if prob[index] > confidence_threshold[index]:
	#		confident_test_count[index] = confident_test_count[index] + 1			
	#
	#train the model on the go
	#clf.partial_fit(x_test, y_test, classes = all_classes)
	# 
 print "tot_rows tested = ", tot_rows
 print "Overall average score = ", float(scores)/denom
#clf.score(x_test_words, y_test)
#x_test_words = vectorizer.transform(x_test)
##clf = clf.fit(x_words,y)
#scores = clf.score(x_test_words,y_test)
#print "Test results:"
#print scores
#print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
#print ""

#The End#
####
#targets = [tweet['class'] for tweet in tweet_reader]
#count_vectorizer = CountVectorizer(ngram_range=(1, 1))          
#counts = count_vectorizer.fit_transform(data)           
#classifier = MultinomialNB()    
#classifier.fit(counts, targets)
