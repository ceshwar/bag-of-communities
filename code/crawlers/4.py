import requests
import time
import sys
import re
from bs4 import BeautifulSoup

board = sys.argv[1]
sys.stderr.write("board is: " + board)

for page in range(1, 10):
	r = requests.get("http://a.4cdn.org/%s/%d.json" % (board, page))
	if r.status_code == 200:
		threads = r.json()
		for i in range(len(threads['threads'])):
			if not 'closed' in  threads['threads'][i]['posts'][0]:
				threadno = threads['threads'][i]['posts'][0]['no']
				time.sleep(1)
				r = requests.get("http://a.4cdn.org/%s/thread/%d.json" % (board, threadno))
				if r.status_code == 200:
					thread = r.json()
					for p in thread['posts']:
						if 'com' in p: 
							comment = p['com'].encode('utf-8')
							comment = re.sub(r'<a href=".*?" class="quotelink">.*?</a>', '', comment)
							comment = re.sub(r'<span class="quote">.*?</span>', '', comment)
							comment = re.sub(r'^\d+$', '', comment)
							comment = re.sub(r'^\d+/.*?', '', comment)
							comment = re.sub(r'(?i)^bump$', '', comment)
							soup = BeautifulSoup(comment)
							comment = ' '.join(soup.stripped_strings).strip()
							try:
								if len(comment) > 0: print(comment.encode("utf-8"))
							except UnicodeEncodeError as e:
								sys.stderr.write("caught a unicode print error, not sure why\n")
					sys.stderr.write("finished thread number %d \n" % threadno)
				else: 
					sys.stderr.write("didn't get 200 response for thread %d" % threadno)
					
	else:
		sys.stderr.write("didn't get 200 response for page %d" % page)
	sys.stderr.write("done with page %d \n" % page)
	time.sleep(1)
