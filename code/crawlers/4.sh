#!/bin/bash

cd /home/ubuntu/mod
# sex
/usr/bin/python 4.py hc > data/sex/tmp/hc.txt
/usr/bin/python 4.py s > data/sex/tmp/s.txt
/usr/bin/python 4.py hm > data/sex/tmp/hm.txt
cp data/sex/all.txt data/sex/all2.txt
cat data/sex/tmp/hc.txt >> data/sex/all2.txt
cat data/sex/tmp/s.txt >> data/sex/all2.txt
cat data/sex/tmp/hm.txt >> data/sex/all2.txt
sort data/sex/all2.txt | uniq > data/sex/all3.txt
mv data/sex/all3.txt data/sex/all.txt
rm data/sex/all2.txt

# racism
/usr/bin/python 4.py pol > data/racism/tmp/pol.txt
cp data/racism/all.txt data/racism/all2.txt
cat data/racism/tmp/pol.txt >> data/racism/all2.txt
sort data/racism/all2.txt | uniq > data/racism/all3.txt 
mv data/racism/all3.txt data/racism/all.txt
rm data/racism/all2.txt

# vulgarity
/usr/bin/python 4.py b > data/vulgarity/tmp/b.txt
cp data/vulgarity/all.txt data/vulgarity/all2.txt
cat data/vulgarity/tmp/b.txt >> data/vulgarity/all2.txt
sort data/vulgarity/all2.txt | uniq > data/vulgarity/all3.txt 
mv data/vulgarity/all3.txt data/vulgarity/all.txt
rm data/vulgarity/all2.txt
