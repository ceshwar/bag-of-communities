import pandas as pd
import numpy as np
import time
import sys
import re
import requests
from bs4 import BeautifulSoup

askme = pd.read_csv('./postdata_askme.txt', sep='\t')
# 7000 posts is approx 100k comments
sub100k = askme.loc[np.random.choice(askme.index, 7000*10, replace=False)]
total = 0

for pid in sub100k.postid:
    try:
        sys.stderr.write("getting http://ask.metafilter.com/%d \n" % int(pid))
        r = requests.get("http://ask.metafilter.com/%d" % int(pid))
        if r.status_code == 200:
            soup = BeautifulSoup(r.text)
            comments = soup.find_all("div", class_="comments", id=re.compile("c\d+"))
            for c in comments:
                post = ' '.join(c.stripped_strings).strip()
                post = post[:post.find("posted by")].strip()
                try:
                    if len(post) > 0: 
                        print(post.encode("utf-8"))
                        total += 1 
                except UnicodeEncodeError as e:
                    sys.stderr.write("caught a unicode print error, not sure why\n")
        else:
            sys.stderr.write("got http error code: %d" & r.status_code)

        sys.stderr.write("  done with %d now \n" % total)
        time.sleep(2)
    except Exception, e:
        pass
